const multirelease = require('multi-semantic-release');
const execa = require('execa');
const path = require('path');

const Status = {
    Released: 'released',
    Skipped: 'skipped'
}

const monorepoRootPath = path.join(__dirname, '..')

const main = async () => {
  const { stdout: workspaceInfo } = await execa('yarn', ['workspaces', 'info']);

  const workspacePackages = Object.entries(JSON.parse(workspaceInfo))
    .map(([, info]) => path.join(monorepoRootPath, info.location, 'package.json'));

    console.log(workspacePackages)
const result = await multirelease(
    workspacePackages,
    undefined,
    undefined,
    { deps: {}, sequentialInit: true, firstParent: true }
  );

  const transformed = result.reduce(
    (acc, { name, result }) => ({ ...acc, [name]: result ? Status.Released : Status.Skipped }),
    {}
  );

  console.table(transformed);
}

main();