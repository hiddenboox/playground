import * as React from "react";
import * as ReactDOM from "react-dom";
import { delay } from '@softbind/utils';

import App from "./App";

(async () => {
await delay(2000)
ReactDOM.render(<App />, document.getElementById("root"));
})()
