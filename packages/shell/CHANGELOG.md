# @softbind/shell [2.2.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@2.1.0...@softbind/shell@2.2.0) (2021-04-30)


### Bug Fixes

* ba ([6e7a40d](https://gitlab.com/hiddenboox/playground/commit/6e7a40db64839d374550bb20df3deeb06276b63a))
* n ([6bdfd79](https://gitlab.com/hiddenboox/playground/commit/6bdfd795e6051c8b4f92f73c73041150471f0429))
* n1 ([1eef78f](https://gitlab.com/hiddenboox/playground/commit/1eef78fe947c11d5aadc075e56bce9804a3975f7))


### Features

* new ([5ef0656](https://gitlab.com/hiddenboox/playground/commit/5ef065630bdc755005602fa09d55e92fd8a5d037))





### Dependencies

* **@softbind/utils:** upgraded to 2.2.0
* **@softbind/babel:** upgraded to 2.2.0

# @softbind/shell [2.2.0-release.2](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@2.2.0-release.1...@softbind/shell@2.2.0-release.2) (2021-04-30)


### Bug Fixes

* n1 ([38bc44c](https://gitlab.com/hiddenboox/playground/commit/38bc44c8312cc31e350a1e1c2b81ae9832944b34))





### Dependencies

* **@softbind/utils:** upgraded to 2.2.0-release.2
* **@softbind/babel:** upgraded to 2.2.0-release.2

# @softbind/shell [2.2.0-release.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@2.1.0...@softbind/shell@2.2.0-release.1) (2021-04-30)


### Features

* new feature ([f7eb2d6](https://gitlab.com/hiddenboox/playground/commit/f7eb2d6aa171ec66b1f65fb5958b4d9aadbec869))





### Dependencies

* **@softbind/utils:** upgraded to 2.2.0-release.1
* **@softbind/babel:** upgraded to 2.2.0-release.1

# @softbind/shell [2.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@2.0.0...@softbind/shell@2.1.0) (2021-04-29)


### Features

* blab ([a836a24](https://gitlab.com/hiddenboox/playground/commit/a836a246c7de0337798ca070fe193850f2ef1e96))
* blab ([d73aa45](https://gitlab.com/hiddenboox/playground/commit/d73aa45bb84b75e8017efd66d1c93589f0cbe195))





### Dependencies

* **@softbind/utils:** upgraded to 2.1.0
* **@softbind/babel:** upgraded to 2.1.0

# @softbind/shell [1.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.0.0...@softbind/shell@1.1.0) (2021-04-29)


### Features

* update versions ([47dff84](https://gitlab.com/hiddenboox/playground/commit/47dff84127e3a8caf9827bee4a10e00d5e2fd00e))





### Dependencies

* **@softbind/utils:** upgraded to 1.1.0

# @softbind/shell 1.0.0 (2021-04-29)


### Bug Fixes

* **shell:** change title ([3514757](https://gitlab.com/hiddenboox/playground/commit/3514757f88717b775a7a2bbec8aeffdc0e0cae15))
* change names ([e3c4848](https://gitlab.com/hiddenboox/playground/commit/e3c4848e6fab8137c7489068d34f75594a76fdc3))
* change names ([2f16ecd](https://gitlab.com/hiddenboox/playground/commit/2f16ecdd8ccdd367ef8c19c5d6cbc65bc5e99ddc))
* docker image ([68c3282](https://gitlab.com/hiddenboox/playground/commit/68c32829d11ad38ae8e67f64ac7f8d8a3f3c236a))


### Features

* use global release conf ([788f85f](https://gitlab.com/hiddenboox/playground/commit/788f85f06d0a65e8ac3e3945e4b0dcb15f9ad99a))
* **ci:** next ([4d2c05b](https://gitlab.com/hiddenboox/playground/commit/4d2c05b91cb9c17f5e06627c6cbc7658028d6b0b))
* **shell:** fix release ([d391871](https://gitlab.com/hiddenboox/playground/commit/d3918711befea851ab6eaaf4f26ef34f92c5ae52))
* **shell:** update release ([cc9a36a](https://gitlab.com/hiddenboox/playground/commit/cc9a36ab6c338c801b0c428f858b3b90d9c7f0ab))
* **shell:** update release ([3945740](https://gitlab.com/hiddenboox/playground/commit/3945740feb85be5bccf490f1667d18568827afff))
* names change ([2fbaf31](https://gitlab.com/hiddenboox/playground/commit/2fbaf3186c1c36e6cbd430abe4ca0346a184ce5d))





### Dependencies

* **@softbind/utils:** upgraded to 1.0.0
* **@softbind/babel:** upgraded to 1.0.0

# @softbind/shell [1.5.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.4.0...@softbind/shell@1.5.0) (2021-04-08)


### Features

* **shell:** decrease delay ([e63fe81](https://gitlab.com/hiddenboox/playground/commit/e63fe81d777ef6e5380d38682801cb7a11661a90))
* **shell:** new ficzer ([92f92bc](https://gitlab.com/hiddenboox/playground/commit/92f92bc1e260fbb0ed90ee44dc33c9741203717d))





### Dependencies

* **@softbind/utils:** upgraded to 1.1.0
* **@softbind/babel:** upgraded to 1.1.0

# @softbind/shell [1.4.0-feature-new-shell.2](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.4.0-feature-new-shell.1...@softbind/shell@1.4.0-feature-new-shell.2) (2021-04-08)


### Features

* **shell:** decrease delay ([e63fe81](https://gitlab.com/hiddenboox/playground/commit/e63fe81d777ef6e5380d38682801cb7a11661a90))

# @softbind/shell [1.4.0-feature-new-shell.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.3.0...@softbind/shell@1.4.0-feature-new-shell.1) (2021-04-08)


### Features

* **shell:** fix release ([d391871](https://gitlab.com/hiddenboox/playground/commit/d3918711befea851ab6eaaf4f26ef34f92c5ae52))
* **shell:** new ficzer ([92f92bc](https://gitlab.com/hiddenboox/playground/commit/92f92bc1e260fbb0ed90ee44dc33c9741203717d))
* **shell:** update release ([cc9a36a](https://gitlab.com/hiddenboox/playground/commit/cc9a36ab6c338c801b0c428f858b3b90d9c7f0ab))
* **shell:** update release ([3945740](https://gitlab.com/hiddenboox/playground/commit/3945740feb85be5bccf490f1667d18568827afff))





### Dependencies

* **@softbind/utils:** upgraded to 1.1.0-feature-new-shell.1
* **@softbind/babel:** upgraded to 1.1.0-feature-new-shell.1

# @softbind/shell [1.3.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.2.3...@softbind/shell@1.3.0) (2021-04-07)


### Features

* **ci:** next ([4d2c05b](https://gitlab.com/hiddenboox/playground/commit/4d2c05b91cb9c17f5e06627c6cbc7658028d6b0b))

## @softbind/shell [1.2.3](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.2.2...@softbind/shell@1.2.3) (2021-03-26)


### Bug Fixes

* **shell:** change title ([3514757](https://gitlab.com/hiddenboox/playground/commit/3514757f88717b775a7a2bbec8aeffdc0e0cae15))

## @softbind/shell [1.2.2](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.2.1...@softbind/shell@1.2.2) (2021-03-26)





### Dependencies

* **@softbind/utils:** upgraded to 1.0.1

## @softbind/shell [1.2.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.2.0...@softbind/shell@1.2.1) (2021-03-26)





### Dependencies

* **@softbind/utils:** upgraded to 1.0.0

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.2.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/shell@1.1.0...@softbind/shell@1.2.0) (2021-03-25)


### Features

* names change ([2fbaf31](https://gitlab.com/hiddenboox/playground/commit/2fbaf3186c1c36e6cbd430abe4ca0346a184ce5d))





# 1.1.0 (2021-03-25)


### Bug Fixes

* change names ([e3c4848](https://gitlab.com/hiddenboox/playground/commit/e3c4848e6fab8137c7489068d34f75594a76fdc3))
* change names ([2f16ecd](https://gitlab.com/hiddenboox/playground/commit/2f16ecdd8ccdd367ef8c19c5d6cbc65bc5e99ddc))
* docker image ([68c3282](https://gitlab.com/hiddenboox/playground/commit/68c32829d11ad38ae8e67f64ac7f8d8a3f3c236a))


### Features

* **ci:** use dynamic gitlab ([356b07c](https://gitlab.com/hiddenboox/playground/commit/356b07c48f915ba390eecb4bf29dc8ccbbc3dda0))
