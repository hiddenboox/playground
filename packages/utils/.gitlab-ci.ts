import { ExtendConfigFunction } from "node-gitlab-ci";
import path from 'path';

const extendConfig: ExtendConfigFunction = async (config) => {
    // Create a job
    config.job("build utils", { stage: "setup", script: ['yarn install --prefer-offline --cache-folder .yarn', `cd ${__dirname}`, "yarn build"]});

    // You can include further files
    // await config.include(__dirname, ["./stage-*.ts"]);
};

export { extendConfig };