declare module '@app/utils' {
  export function delay(ms: number): Promise<void>;
}