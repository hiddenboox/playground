# @softbind/utils [2.2.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@2.1.0...@softbind/utils@2.2.0) (2021-04-30)


### Bug Fixes

* ba ([6e7a40d](https://gitlab.com/hiddenboox/playground/commit/6e7a40db64839d374550bb20df3deeb06276b63a))
* n ([6bdfd79](https://gitlab.com/hiddenboox/playground/commit/6bdfd795e6051c8b4f92f73c73041150471f0429))
* n1 ([1eef78f](https://gitlab.com/hiddenboox/playground/commit/1eef78fe947c11d5aadc075e56bce9804a3975f7))


### Features

* new ([5ef0656](https://gitlab.com/hiddenboox/playground/commit/5ef065630bdc755005602fa09d55e92fd8a5d037))





### Dependencies

* **@softbind/babel:** upgraded to 2.2.0

# @softbind/utils [2.2.0-release.2](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@2.2.0-release.1...@softbind/utils@2.2.0-release.2) (2021-04-30)


### Bug Fixes

* n1 ([38bc44c](https://gitlab.com/hiddenboox/playground/commit/38bc44c8312cc31e350a1e1c2b81ae9832944b34))





### Dependencies

* **@softbind/babel:** upgraded to 2.2.0-release.2

# @softbind/utils [2.2.0-release.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@2.1.0...@softbind/utils@2.2.0-release.1) (2021-04-30)


### Features

* new feature ([f7eb2d6](https://gitlab.com/hiddenboox/playground/commit/f7eb2d6aa171ec66b1f65fb5958b4d9aadbec869))





### Dependencies

* **@softbind/babel:** upgraded to 2.2.0-release.1

# @softbind/utils [2.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@2.0.0...@softbind/utils@2.1.0) (2021-04-29)


### Bug Fixes

* yarn before ([2f28855](https://gitlab.com/hiddenboox/playground/commit/2f28855e2062f2d57a5fc4489602bdeb7f3191a2))


### Features

* blab ([a836a24](https://gitlab.com/hiddenboox/playground/commit/a836a246c7de0337798ca070fe193850f2ef1e96))
* blab ([d73aa45](https://gitlab.com/hiddenboox/playground/commit/d73aa45bb84b75e8017efd66d1c93589f0cbe195))
* yarn script ([071608b](https://gitlab.com/hiddenboox/playground/commit/071608bf62299dc63bb22adf2a2115f17b576604))





### Dependencies

* **@softbind/babel:** upgraded to 2.1.0

# @softbind/utils [1.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@1.0.0...@softbind/utils@1.1.0) (2021-04-29)


### Features

* update versions ([47dff84](https://gitlab.com/hiddenboox/playground/commit/47dff84127e3a8caf9827bee4a10e00d5e2fd00e))

# @softbind/utils 1.0.0 (2021-04-29)


### Bug Fixes

* **ci:** publish pkg ([7f9fd3e](https://gitlab.com/hiddenboox/playground/commit/7f9fd3e867b5f943abff4d6fc1642aac46f0f8eb))
* **ci:** update publish ([44cddbc](https://gitlab.com/hiddenboox/playground/commit/44cddbc40c68914e441870019b05fdf8ccf0f73e))
* **ci:** yarn ([d2d0eb9](https://gitlab.com/hiddenboox/playground/commit/d2d0eb9d6946feb934c2bb48dbb6752fefaf4689))
* **delay:** increase ms ([cfbaaa4](https://gitlab.com/hiddenboox/playground/commit/cfbaaa4e81f8246fb6852735bc98fb71adf60ded))
* **utils:** increase ms ([3359b45](https://gitlab.com/hiddenboox/playground/commit/3359b457520aec7ffe083dd720eeb97c6046820d))
* **utils:** remove braces ([3301b15](https://gitlab.com/hiddenboox/playground/commit/3301b151221ed4289d43ef854f529daa26383d74))


### Features

* use global release conf ([788f85f](https://gitlab.com/hiddenboox/playground/commit/788f85f06d0a65e8ac3e3945e4b0dcb15f9ad99a))
* **ci:** publish utils ([5c2a747](https://gitlab.com/hiddenboox/playground/commit/5c2a74712ef3427e49ad08cbf0c5c1c16b81259b))

# @softbind/utils [1.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@1.0.1...@softbind/utils@1.1.0) (2021-04-08)


### Features

* **ci:** release all ([a640fb3](https://gitlab.com/hiddenboox/playground/commit/a640fb3287bf66ba533895a87c8fd6fbdfc27a40))

# @softbind/utils [1.1.0-feature-new-shell.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@1.0.1...@softbind/utils@1.1.0-feature-new-shell.1) (2021-04-08)


### Features

* **ci:** release all ([a640fb3](https://gitlab.com/hiddenboox/playground/commit/a640fb3287bf66ba533895a87c8fd6fbdfc27a40))

## @softbind/utils [1.0.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/utils@1.0.0...@softbind/utils@1.0.1) (2021-03-26)


### Bug Fixes

* **utils:** increase ms ([3359b45](https://gitlab.com/hiddenboox/playground/commit/3359b457520aec7ffe083dd720eeb97c6046820d))

# @softbind/utils 1.0.0 (2021-03-26)


### Bug Fixes

* **ci:** publish pkg ([7f9fd3e](https://gitlab.com/hiddenboox/playground/commit/7f9fd3e867b5f943abff4d6fc1642aac46f0f8eb))
* **ci:** update publish ([44cddbc](https://gitlab.com/hiddenboox/playground/commit/44cddbc40c68914e441870019b05fdf8ccf0f73e))
* **ci:** yarn ([d2d0eb9](https://gitlab.com/hiddenboox/playground/commit/d2d0eb9d6946feb934c2bb48dbb6752fefaf4689))
* **delay:** increase ms ([cfbaaa4](https://gitlab.com/hiddenboox/playground/commit/cfbaaa4e81f8246fb6852735bc98fb71adf60ded))
* **utils:** remove braces ([3301b15](https://gitlab.com/hiddenboox/playground/commit/3301b151221ed4289d43ef854f529daa26383d74))


### Features

* **ci:** publish utils ([5c2a747](https://gitlab.com/hiddenboox/playground/commit/5c2a74712ef3427e49ad08cbf0c5c1c16b81259b))

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.1.0-next.0 (2021-03-25)


### Bug Fixes

* **ci:** publish pkg ([7f9fd3e](https://gitlab.com/hiddenboox/playground/commit/7f9fd3e867b5f943abff4d6fc1642aac46f0f8eb))
* **ci:** update publish ([44cddbc](https://gitlab.com/hiddenboox/playground/commit/44cddbc40c68914e441870019b05fdf8ccf0f73e))
* **ci:** yarn ([d2d0eb9](https://gitlab.com/hiddenboox/playground/commit/d2d0eb9d6946feb934c2bb48dbb6752fefaf4689))
* **delay:** increase ms ([cfbaaa4](https://gitlab.com/hiddenboox/playground/commit/cfbaaa4e81f8246fb6852735bc98fb71adf60ded))


### Features

* **ci:** publish utils ([5c2a747](https://gitlab.com/hiddenboox/playground/commit/5c2a74712ef3427e49ad08cbf0c5c1c16b81259b))
