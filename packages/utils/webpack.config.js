const path = require("path");
const webpack = require("webpack");

module.exports = {
  target: ["web", "es2020"],
  output: {
      path: path.resolve(__dirname, './dist'),
      filename: `utils.umd.js`,
      library: {
          type: "umd",
      }
  },
}
