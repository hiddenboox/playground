import { ExtendConfigFunction } from "node-gitlab-ci";
import path from 'path';

const extendConfig: ExtendConfigFunction = async (config) => {
    config.job("deploy utils", { stage: "deploy", script: [`cd ${__dirname}`, "yarn version prerelease --preid=next", "yarn publish --tag next"] })
};

export { extendConfig };