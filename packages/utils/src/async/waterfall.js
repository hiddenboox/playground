export const waterfall = (promises) =>
  promises.reduce((acc, next) =>
    acc.then((value) => next().then((nextVal) =>
      [].concat(value, nextVal).filter(Boolean))),
  Promise.resolve());


