# @softbind/navigation [2.2.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@2.1.0...@softbind/navigation@2.2.0) (2021-04-30)


### Bug Fixes

* ba ([6e7a40d](https://gitlab.com/hiddenboox/playground/commit/6e7a40db64839d374550bb20df3deeb06276b63a))
* n ([6bdfd79](https://gitlab.com/hiddenboox/playground/commit/6bdfd795e6051c8b4f92f73c73041150471f0429))
* n1 ([1eef78f](https://gitlab.com/hiddenboox/playground/commit/1eef78fe947c11d5aadc075e56bce9804a3975f7))


### Features

* new ([5ef0656](https://gitlab.com/hiddenboox/playground/commit/5ef065630bdc755005602fa09d55e92fd8a5d037))





### Dependencies

* **@softbind/babel:** upgraded to 2.2.0

# @softbind/navigation [2.2.0-release.2](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@2.2.0-release.1...@softbind/navigation@2.2.0-release.2) (2021-04-30)


### Bug Fixes

* n1 ([38bc44c](https://gitlab.com/hiddenboox/playground/commit/38bc44c8312cc31e350a1e1c2b81ae9832944b34))





### Dependencies

* **@softbind/babel:** upgraded to 2.2.0-release.2

# @softbind/navigation [2.2.0-release.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@2.1.0...@softbind/navigation@2.2.0-release.1) (2021-04-30)


### Features

* new feature ([f7eb2d6](https://gitlab.com/hiddenboox/playground/commit/f7eb2d6aa171ec66b1f65fb5958b4d9aadbec869))





### Dependencies

* **@softbind/babel:** upgraded to 2.2.0-release.1

# @softbind/navigation [2.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@2.0.0...@softbind/navigation@2.1.0) (2021-04-29)


### Features

* blab ([a836a24](https://gitlab.com/hiddenboox/playground/commit/a836a246c7de0337798ca070fe193850f2ef1e96))
* blab ([d73aa45](https://gitlab.com/hiddenboox/playground/commit/d73aa45bb84b75e8017efd66d1c93589f0cbe195))





### Dependencies

* **@softbind/babel:** upgraded to 2.1.0

# @softbind/navigation [1.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@1.0.0...@softbind/navigation@1.1.0) (2021-04-29)


### Features

* update versions ([47dff84](https://gitlab.com/hiddenboox/playground/commit/47dff84127e3a8caf9827bee4a10e00d5e2fd00e))

# @softbind/navigation 1.0.0 (2021-04-29)


### Bug Fixes

* change names ([e3c4848](https://gitlab.com/hiddenboox/playground/commit/e3c4848e6fab8137c7489068d34f75594a76fdc3))
* change names ([2f16ecd](https://gitlab.com/hiddenboox/playground/commit/2f16ecdd8ccdd367ef8c19c5d6cbc65bc5e99ddc))
* change title ([c935e80](https://gitlab.com/hiddenboox/playground/commit/c935e80b23b3fd6ae7f7a5c0c274eb7cc96bb331))


### Features

* names change ([2fbaf31](https://gitlab.com/hiddenboox/playground/commit/2fbaf3186c1c36e6cbd430abe4ca0346a184ce5d))
* use global release conf ([788f85f](https://gitlab.com/hiddenboox/playground/commit/788f85f06d0a65e8ac3e3945e4b0dcb15f9ad99a))





### Dependencies

* **@softbind/babel:** upgraded to 1.0.0

# @softbind/navigation [1.2.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@1.1.1...@softbind/navigation@1.2.0) (2021-04-08)


### Features

* **ci:** release all ([a640fb3](https://gitlab.com/hiddenboox/playground/commit/a640fb3287bf66ba533895a87c8fd6fbdfc27a40))





### Dependencies

* **@softbind/babel:** upgraded to 1.1.0

# @softbind/navigation [1.2.0-feature-new-shell.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@1.1.1...@softbind/navigation@1.2.0-feature-new-shell.1) (2021-04-08)


### Features

* **ci:** release all ([a640fb3](https://gitlab.com/hiddenboox/playground/commit/a640fb3287bf66ba533895a87c8fd6fbdfc27a40))





### Dependencies

* **@softbind/babel:** upgraded to 1.1.0-feature-new-shell.1

## @softbind/navigation [1.1.1](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@1.1.0...@softbind/navigation@1.1.1) (2021-03-26)


### Bug Fixes

* change title ([c935e80](https://gitlab.com/hiddenboox/playground/commit/c935e80b23b3fd6ae7f7a5c0c274eb7cc96bb331))

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.1.0](https://gitlab.com/hiddenboox/playground/compare/@softbind/navigation@1.0.1...@softbind/navigation@1.1.0) (2021-03-25)


### Features

* names change ([2fbaf31](https://gitlab.com/hiddenboox/playground/commit/2fbaf3186c1c36e6cbd430abe4ca0346a184ce5d))





## 1.0.1 (2021-03-25)


### Bug Fixes

* change names ([e3c4848](https://gitlab.com/hiddenboox/playground/commit/e3c4848e6fab8137c7489068d34f75594a76fdc3))
* change names ([2f16ecd](https://gitlab.com/hiddenboox/playground/commit/2f16ecdd8ccdd367ef8c19c5d6cbc65bc5e99ddc))
