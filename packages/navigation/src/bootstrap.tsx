import * as React from "react";
import * as ReactDom from "react-dom";

import { App } from "./App";

ReactDom.render(<App title="Demo2" />, document.getElementById("root"));
