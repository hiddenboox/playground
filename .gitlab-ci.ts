import { Config, CreateConfigFunction } from "node-gitlab-ci";

const createConfig: CreateConfigFunction = async () => {
    const config = new Config();

    config.stages("install", "setup", "test", "build", "deploy");
    const envs = process.env

    const cache = {
        paths: [
            "node_modules",
            "packages/*/node_modules"
        ]
    }


    config.defaults({
        image: "node:lts-stretch",
        cache
    });

    config.job("install node_modules", {
        stage: "install", script: ["yarn install --prefer-offline"], cache: { ...cache, policy: 'pull-push' }
    })

    config.job("deploy packages", {
        stage: "deploy",
        script: [
            "DEBUG=msr:commitsFilter yarn release"
        ],
        rules: [{
            if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
            // @ts-ignore
            when: 'always',
          }, {
            if: '$CI_COMMIT_BRANCH == "release"',
            // @ts-ignore
            when: 'always'
          }, {
            // @ts-ignore
            when: 'never'
          }],
        cache: { ...cache, policy: 'pull' }
    })

    // Allows you to include further configurations by glob patterns
    // await config.include(__dirname, ["packages/*/.gitlab-ci.ts"]);

    return config;
};

export { createConfig };