# 1.0.0 (2021-03-26)


### Bug Fixes

* change title ([c935e80](https://gitlab.com/hiddenboox/playground/commit/c935e80b23b3fd6ae7f7a5c0c274eb7cc96bb331))
* **delay:** increase ms ([cfbaaa4](https://gitlab.com/hiddenboox/playground/commit/cfbaaa4e81f8246fb6852735bc98fb71adf60ded))
* cc added ([f02a7a3](https://gitlab.com/hiddenboox/playground/commit/f02a7a305f75da08fb92cf8cd205f342c6fd04ad))
* change names ([e3c4848](https://gitlab.com/hiddenboox/playground/commit/e3c4848e6fab8137c7489068d34f75594a76fdc3))
* change names ([2f16ecd](https://gitlab.com/hiddenboox/playground/commit/2f16ecdd8ccdd367ef8c19c5d6cbc65bc5e99ddc))
* docker image ([68c3282](https://gitlab.com/hiddenboox/playground/commit/68c32829d11ad38ae8e67f64ac7f8d8a3f3c236a))
* lerna ([3a8ae16](https://gitlab.com/hiddenboox/playground/commit/3a8ae168f1b1f0e39f1e5e56b8f1d142c613ffdd))
* **ci:** fix gitpod ([f102fee](https://gitlab.com/hiddenboox/playground/commit/f102fee2276bf7b53a2822c78172d17a0f2e881a))
* **ci:** publish pkg ([7f9fd3e](https://gitlab.com/hiddenboox/playground/commit/7f9fd3e867b5f943abff4d6fc1642aac46f0f8eb))
* **ci:** update publish ([44cddbc](https://gitlab.com/hiddenboox/playground/commit/44cddbc40c68914e441870019b05fdf8ccf0f73e))
* **ci:** yarn ([d2d0eb9](https://gitlab.com/hiddenboox/playground/commit/d2d0eb9d6946feb934c2bb48dbb6752fefaf4689))


### Features

* names change ([2fbaf31](https://gitlab.com/hiddenboox/playground/commit/2fbaf3186c1c36e6cbd430abe4ca0346a184ce5d))
* **ci:** publish utils ([5c2a747](https://gitlab.com/hiddenboox/playground/commit/5c2a74712ef3427e49ad08cbf0c5c1c16b81259b))
* **ci:** use dynamic gitlab ([356b07c](https://gitlab.com/hiddenboox/playground/commit/356b07c48f915ba390eecb4bf29dc8ccbbc3dda0))
* use pnpm instead of yarn ([3eac495](https://gitlab.com/hiddenboox/playground/commit/3eac4959fd801142e07288712f6024f3da9e157a))
