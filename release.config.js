const { CI_COMMIT_BRANCH } = process.env;

module.exports = {
  branches: [
    'main',
    { name: 'release', prerelease: true, channel: 'next' },
  ],
  plugins: CI_COMMIT_BRANCH === 'main' ? ['@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/changelog',
    '@semantic-release/npm',
    '@semantic-release/git',
    '@semantic-release/gitlab'
  ] : [
    '@semantic-release/commit-analyzer',
  ],
  noCi: true,
  debug: true,
};
